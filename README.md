# Zabbix passive checks

This crate interacts with a Zabbix agent and queries it.

```rust
let result: String = get_metric("127.0.0.1:10050".parse().unwrap(), "agent.ping").expect("Failed to get metric");
```
